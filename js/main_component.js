'use strict';
angular.module('mainComponent', ['ngComponentRouter', 'invoice','waybill','waybillhistory']);
// Register `phoneList` component, along with its associated controller and template
angular.
        module('mainComponent')
        .config(function ($locationProvider) {
            $locationProvider.html5Mode(false);
        })

        .value('$routerRootComponent', 'mainComponent')
        .component('mainComponent', {
           templateUrl: '/bootstrap/templates/main_controller.html',
            $routeConfig: [
                {path: '/invoice', name: 'Invoice', component: 'mInvoice', useAsDefault: true},
                {path: '/waybill', name: 'Waybill', component: 'waybillComponent'},
                {path: '/waybillhistory', name: 'WaybillHistory', component: 'waybillhistoryComponent'}
                //,{path: '/heroes/...', name: 'Heroes', component: 'heroes'}
            ]


        });
