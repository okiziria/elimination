'use strict';
angular.module('chamowera', []);
// Register `phoneList` component, along with its associated controller and template
angular.
        module('chamowera').
        component('mChamowera', {
            templateUrl: '/bootstrap/templates/chamowera.html',
            bindings: {$router: '<'},
            controllerAs: 'vv',
            controller: ['$http', function MChamoweraController($http) {

                    var self = this;
                    self.drugs = '';
                    self.selectedDrug = '';
                    self.deleted = [];
                    self.invoices = [];
                    self.selectedInvoice = '';
                    self.showCvantity = false;
                    self.disableButton = false;
                    self.selectedCvantity = '';
                    self.selectedDate = '';
                    self.types = [
                        {name: 'ჩამოწერა - გადანგურების მიზნით', value: 1},
                        {name: 'ჩამოწერა - დონაციის მიზნით', value: 2},
                        {name: 'ჩამოწერა - უკან დაბრუნების მიზნით', value: 3},
                        {name: 'ჩამოწერა - რეალიზაციის მიზნით', value: 4}

                    ];
                    self.selectedType = 1;



                    self.$onInit = function () {
                        self.getData();
                    };



                    self.getData = function () {
                        var url = '/index.php?m=angularajax&c=chamowera&t=getData';
                        self.showCvantity = false;
                        console.log('clickedOn submit');
                        $http.post(url, {}).then(function (response) {
                            console.log(response.data)
                            if (response.data.status)
                            {
                                self.drugs = response.data.data.drugs;
                                self.deleted = response.data.data.deleted;
                                //self.ajaxMessage = response.data.message;
                            } else
                            {
                                self.ajaxMessage = response.data.message;
                            }
                        });
                    };


                    self.selectDrug = function () {
                        self.showCvantity = false;
                        var url = '/index.php?m=angularajax&c=chamowera&t=getInvoices';
                        console.log(self.selectedDrug);
                        $http.post(url, {id: self.selectedDrug}).then(function (response) {
                            console.log(response.data)
                            if (response.data.status)
                            {
                                self.invoices = response.data.data.invoices;
                                if (self.invoices.length)
                                {

                                }
                                self.ajaxMessage = response.data.message;
                            } else
                            {
                                self.invoices = []
                                self.ajaxMessage = response.data.message;
                            }
                        });

                    };
                    self.selectInvoice = function () {

                        self.showCvantity = true;


                    };


                    self.save = function ()
                    {
                        self.ajaxMessage = '';

                        self.showCvantity = false;
                        self.disableButton = true;
                        var url = '/index.php?m=angularajax&c=chamowera&t=save';
                        console.log(self.selectedDrug);
                        $http.post(url, {id: self.selectedInvoice, count: self.selectedCvantity, date: self.selectedDate, type: self.selectedType}).then(function (response) {
                            console.log(response.data)
                            if (response.data.status)
                            {
                                self.disableButton = false;
                                self.reset();
                                self.getData();
                                self.ajaxMessage = response.data.message;
                            } else
                            {
                                self.disableButton = false;
                                self.ajaxMessage = response.data.message;
                            }
                        });



                    };

                    self.reset = function () {
                        self.invoices = [];
                        self.selectedInvoice = '';
                        self.showCvantity = false;
                        self.disableButton = false;
                        self.selectedCvantity = 0;
                        self.selectedDrug = '';
                    };

                }]
        })
        ;
