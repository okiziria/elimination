'use strict';
angular.module('mainComponent', ['ngComponentRouter', 'chamowera']);
// Register `phoneList` component, along with its associated controller and template
angular.
        module('mainComponent')
        .config(function ($locationProvider) {
            $locationProvider.html5Mode(false);
        })

        .value('$routerRootComponent', 'mainComponent')
        .component('mainComponent', {
           templateUrl: '/bootstrap/templates/chamowera_main_controller.html',
            $routeConfig: [
                {path: '/chamowera', name: 'Chamowera', component: 'mChamowera', useAsDefault: true},
                //{path: '/waybill', name: 'Waybill', component: 'waybillComponent'},
               // {path: '/waybillhistory', name: 'WaybillHistory', component: 'waybillhistoryComponent'}
                //,{path: '/heroes/...', name: 'Heroes', component: 'heroes'}
            ]


        });
