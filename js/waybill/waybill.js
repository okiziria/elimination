'use strict';
angular.module('waybill', []);
angular.module('waybill').
        component('waybillComponent', {
            templateUrl: '/bootstrap/js/waybill/default.html',
            bindings: {$router: '<'},
            controller: ['$http', function waibillComponentController($http) {

                    var self = this;
                    self.ntnCode = '';
                    self.ajaxMessage = '';
                    self.areInputsDisabled = false;
                    self.finedDrugTransfers = new Array();
                    self.checkSendButton = function ()
                    {
                        if (self.ntnCode === '')
                        {
                            return true;
                        }
                        return false;
                    };




                    self.getData = function () {
                        var url = '/index.php?m=angularajax&c=invoice&t=checkwaybill';
                        console.log('clickedOn submit');
                        $http.post(url, {ntnkey: self.ntnCode}).then(function (response) {
                            console.log(response.data.status);
                            console.log(response.data.data);
                            if (response.data.status)
                            {
                                self.setArray(response.data.data);
                                self.areInputsDisabled = true;
                                self.ajaxMessage = response.data.message;
                            } else {
                                self.ajaxMessage = response.data.message;
                            }


                        });
                    };
                    self.deleteData = function () {

                        var url = '/index.php?m=angularajax&c=invoice&t=deleteinvoice';
                        console.log('clickedOn submit');
                        $http.post(url, {ntnkey: self.ntnCode}).then(function (response) {
                            if (response.data.status)
                            {
                                self.ajaxMessage = response.data.message;
                            } else {
                                self.ajaxMessage = response.data.message;
                            }
                        });

                    };
                    self.reset = function () {

                        self.ntnCode = '';
                        self.areInputsDisabled = false;
                        self.finedDrugTransfers = new Array();
                        self.ajaxMessage = '';
                    };

                    self.setArray = function (data) {
                        console.log(data);
                        self.finedDrugTransfers = data;
                    };

                }]
        })
        ;
